  <footer class="c-footer">
    <div class="o-container">
      <a href="#" class="c-footer__logo">
        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icons.svg#logo"></use></svg>
      </a>

      <!-- social -->
      <ul class="c-social-links">
        <li class="c-social-links__item">
          <a href="https://www.facebook.com/comissaoparitariacnc" target="_blank">
            <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icons.svg#facebook"></use></svg>
          </a>
        </li>
        <li class="c-social-links__item">
          <a href="https://www.instagram.com/campanhavoltandopracasa" target="_blank">
            <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icons.svg#instagram"></use></svg>
          </a>
        </li>
        <li class="c-social-links__item">
          <a href="https://twitter.com/voltandopcasa" target="_blank">
            <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icons.svg#twitter"></use></svg>
          </a>
        </li>
      </ul>
    </div>

    <div class="c-footer__bar">
      <a href="https://www.cappen.com/" target="_blank">
        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icons.svg#cappen"></use></svg>
      </a>
    </div>
  </footer>

  <?php wp_footer(); ?>

  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/app.js"></script>
</body>
</html>