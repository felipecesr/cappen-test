<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Voltando Pra Casa
 * @since Voltando Pra Casa 1.0
 */

get_header(); ?>

<main class="c-intro-wrap">
  <div class="c-intro-text">
    <h1 class="c-intro-text__title">Quem olha não esquece <strong>jamais</strong>.</h1>
    <p class="c-intro-text__resume">Os Aflitos foram palco para muitas emoções. Muitos gols. Goleadas. Jogos históricos. Foi onde o Náutico construiu várias das melhores campanhas como mandante, em todo o Brasil. Foi onde vimos nascer ídolos, como Kuki. E construímos uma grande história com sangue, suor e lágrimas. Neste espaço vamos celebrar as memórias do nosso estádio, enquanto preparamos o coração para o nosso retorno.</p>
  </div>
  <div class="c-intro-wrap__background">
    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/aflitos.jpg" alt="Estádio dos Aflitos">
  </div>
</main>

<section class="c-media-gallery">
  <div class="o-container">
    <h1 class="o-heading">Galeria de <strong>vídeos</strong>.</h1>
    <p class="o-resume">Confira abaixo alguns momentos históricos vividos no Caldeirão.</p>

    <!-- list videos -->
    <ul>
      <?php if ( have_posts() ) : $posts = new WP_Query( array( 'order' => 'ASC' ) ); while ( $posts->have_posts() ) : $posts->the_post(); ?>
      <li>
        <a href="#" class="c-media-item">
          <?php the_post_thumbnail('full' ,array('class' => 'c-media-item__photo')); ?>
        </a>
        <p><?php the_title(); ?></p>
      </li>
      <?php endwhile; endif; ?>
    </ul>
  </div>
</section>

<section class="c-facts">
  <div class="o-container">
    <h1 class="o-heading">Fatos e <strong>curiosidades</strong>.</h1>
    <p class="o-resume">O Eládio de Barros Carvalho, ou simplesmente Aflitos tem personagens e momentos únicos. Descubra um pouco mais abaixo sobre cada um.</p>

    <ul class="c-article-list">
      <li>

        <article class="c-article-block">
          <figure class="c-article-block__photo">
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/avenida-alvirrubra.jpg" alt="Avenida alvirrubra">
          </figure>
          <div class="c-article-caption">
            <div class="c-article-caption__counter">1<span>/14</span></div>
            <div class="c-article-content">
              <h2 class="c-article-content__title">Avenida Alvirrubra</h2>
              <p class="c-article-content__resume">Avenida Alvirrubra O Eládio de Barros Carvalho, ou simplesmente Aflitos tem personagens e momentos únicos. Descubra um pouco mais abaixo sobre cada um.</p>
            </div>
          </div>
        </article>

      </li>
    </ul>
  </div>
</section>

<div class="c-banner-divider"></div>

<section class="c-mailing">
  <div class="o-container">
    <h1 class="o-heading">Acompanhe a <strong>Evolução</strong></h1>

    <form class="c-sendmail">
      <input type="email" class="c-sendmail__input" placeholder="Cadastre aqui o seu E-mail">
      <button type="submit" class="c-sendmail__button">
        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icons.svg#send"></use></svg>
      </button>
    </form>
  </div>
</section>

<?php get_footer(); ?>