<?php

// support post thumnails
add_theme_support( 'post-thumbnails' );

// modify post thumbnail tag
function modify_post_thumbnail_html($html, $post_id, $post_thumbnail_id, $size, $attr) {
    $id = get_post_thumbnail_id();
    $src = wp_get_attachment_image_src($id, $size);
    $alt = get_the_title($id);
    $class = (!isset($attr['class'])) ? '' : $attr['class'];

    if (empty($class))
        $html = '<img src="' . $src[0] . '" alt="' . $alt . '" />';
    else
        $html = '<img src="' . $src[0] . '" alt="' . $alt . '" class="' . $class . '" />';

    return $html;
}

add_filter('post_thumbnail_html', 'modify_post_thumbnail_html', 99, 5);

// register menu
function wpb_custom_new_menu() {
    register_nav_menu('primary-menu',__( 'Primary Menu' ));
}

add_action( 'init', 'wpb_custom_new_menu' );

// menu no ul
function wp_nav_menu_no_ul($options)
{
    $menu = wp_nav_menu($options);
    echo preg_replace(array(
        '#^<ul[^>]*>#',
        '#</ul>$#'
    ), '', $menu);

}

function fall_back_menu(){
    return;
}

// add class li menu
function add_classes_on_li($classes, $item, $args) {
    $classes[] = 'c-menu-list__item';
    return $classes;
}

add_filter('nav_menu_css_class','add_classes_on_li',1,3);
