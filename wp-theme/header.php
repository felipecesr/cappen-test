<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Voltando Pra Casa
 * @since Voltando Pra Casa 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Campanha "Voltando Pra Casa"</title>

  <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/css/app.css">

  <?php wp_head(); ?>
</head>
<body>
  <nav class="c-menu-nav">
    <!-- logo -->
    <a href="/" class="c-menu-nav__logo">
      <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icons.svg#logo"></use></svg>
    </a>

    <!-- btn-menu -->
    <button class="o-hamburger"></button>

    <!-- menu -->
    <ul class="c-menu-list">
      <?php
        wp_nav_menu_no_ul(array(
          'echo' => false,
          'container' => false,
          'theme_location' => 'primary-menu',
          'fallback_cb'=> 'fall_back_menu'
        ));
      ?>
      <li class="c-menu-list__item">
        <a href="#" class="o-button">
          <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icons.svg#doacao"></use></svg>
          <span>Ajude agora</span>
        </a>
      </li>
    </ul>
  </nav>