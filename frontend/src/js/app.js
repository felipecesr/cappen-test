(function() {
  'use strict'

  var $ = document.querySelector.bind(document);

  var button = $('.o-hamburger');
  var body = document.documentElement;

  button.addEventListener('click', function() {
    return (body.className === 'active') ? body.className = '' : body.className = 'active';
  });

  body.addEventListener('click', function(e) {
    if (e.target === document.documentElement) {
      body.className = '';
    }
  });
})();
